﻿
var players = []; // Players array with name and score.
var active_players = 0; // Number of players that haven't lost yet.
var current_turn = -1; // Index for players array.
var INIT_SCORE = 10; // New player's starting score.

var verbs = [
        "Hug",
        "Take a drink",
        "Kiss",
        "Dance", 
        "Twerk",
        "Sing",
        "Crawl on the floor",
        "Spin around",
        "Run around",
        "Confess your true feelings",
        "Recite poetry",
        "Jump"
];
var nouns = [
        "(with/to) the person next to you",
        "(with/to) any other player",
        "(with/to) every other player",
        "(with/to) anything",
        "(with/to) all the guys playing",
        "(with/to) all the girls playing",
        "like a dog",
        "like a baby",
        "furiously",
        "until another player kisses you",
        "(with/to) a chair",
        "backwards",
        "for a whole minute",
        "until your next turn"
];

window.onload = function() {
    document.getElementById("yes_btn").style.display = "none";
    document.getElementById("no_btn").style.display = "none";
    document.getElementById("what_btn").style.display = "none";
    document.getElementById("new_game").style.display = "none";
    document.getElementById("spin").style.visibility = "hidden";
}

// When only one player (or none) is left.
function game_over() {
    var winner = "Nobody";
    for(var i = 0; i < players.length; i++) {
        if(players[i] != null) {
            winner = players[i].name;
        }
    }
    document.getElementById("challenge").value = "";
    document.getElementById("whose_turn").innerHTML = "<br/>" + winner + " won the game!!! Congratulations!";
    document.getElementById("yes_btn").style.display = "none";
    document.getElementById("no_btn").style.display = "none";
    document.getElementById("what_btn").style.display = "none";
    document.getElementById("new_game").style.display = "block";
}

// Called after player either completes or turns down challenge.
function next_turn() {
    // Current user's performance feedback.
    var feedback = "";
    if(current_turn >= 0) {
        if(players[current_turn].score <= 0) {
            feedback = players[current_turn].name + " has lost the game.<br/>";
            players[current_turn] = null;
            active_players--;
            if(active_players <= 1) {
                game_over();
                return true;
            }
        } else {
            feedback = players[current_turn].name + "'s score: " + players[current_turn].score + ".<br/>";
        }
    }

    // Search next valid player.
    do {
        current_turn++;
        if (current_turn == players.length) {
            current_turn = 0;
        }
    } while(players[current_turn] == null);

    // Prepare interface for next player.
    document.getElementById("yes_btn").style.display = "none";
    document.getElementById("no_btn").style.display = "none";
    document.getElementById("what_btn").style.display = "block";
    document.getElementById("challenge").value = "";
    feedback += "It's " + players[current_turn].name + "'s turn now.";
    document.getElementById("whose_turn").innerHTML = feedback;
}

// Check if player's name is already on the game.
function is_player(name) {
    for(var i = 0; i < players.length; i++) {
        if(players[i] != null && name == players[i].name) {
            return true;
        }
    }
    return false;
}

// When 'Add' button is pressed.
function add_player() {
    var name = document.getElementById("player_name").value;
    if(name.length === 0 || !name.trim()) {
        document.getElementById("new_player_feedback").innerHTML = "Invisible players not allowed! Write your name!";
        return false;
    }
    if(!is_player(name)) {
        players.push({ name: name, score: INIT_SCORE});
        document.getElementById("new_player_feedback").innerHTML = name + "! Welcome to the game!";
        active_players++;
    } else {
        document.getElementById("new_player_feedback").innerHTML = name + ", you're already on the game. You can't add yourself twice!";
        return false;
    }
    document.getElementById("player_name").value = "";
    
    if(players.length == 1) {
        document.getElementById("spin").style.visibility = "visible";
        next_turn();
    }
}

// When 'What do we do now?' button is preseed.
function what() {
    document.getElementById("yes_btn").style.display = "block";
    document.getElementById("no_btn").style.display = "block";
    document.getElementById("what_btn").style.display = "none";

    var random_verb = Math.floor(Math.random()*verbs.length);
    var random_noun = Math.floor(Math.random()*nouns.length);
    var challenge = verbs[random_verb] + " " + nouns[random_noun] + ".";
    document.getElementById("challenge").value = challenge;
}

// When 'I did it!' button is preseed.
function challenge_done() {
    next_turn();
}

// When 'I couldn't do it!' button is preseed.
function no_thanks() {
    players[current_turn].score--;
    next_turn();
}

// When 'New game' is pressed.
function new_game() {
    window.location.reload(); // :P
}

// Display game rules.
function help() {
    window.alert("Rules:\n- Add your name and click on 'Add player'.\n- Click on 'What do we do now' when it's your turn.\n- Do as instructed.\n- If you don't, you lose a point. If you get to zero, you lose!\n- Enjoy your game!!!");
}
