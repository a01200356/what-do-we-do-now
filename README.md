**What Do We Do Now?**

        A web app and a social game to challenge people to do random fun things. 

        Specially recommended for those moments when you are with friends and you are asking yourselves 'What do we do now?'.

        Created for the 2015 Global Game Jam. Release entry here: http://globalgamejam.org/2015/games/what-do-we-do-now-32

**Requirements**

        Any browser that isn't way too old.

        People looking to have fun.

**Instructions**

        Just open index.html with browser.

        Follow the instructions there.


**License**

        This game and all content in this file is licensed under the Attribution-Noncommercial-Share Alike 3.0 version of the Creative Commons License. For reference the license is given below and can also be found at http://creativecommons.org/licenses/by-nc-sa/3.0/

        The template has its own license, which is available here: http://codepen.io/azureknight/pen/ypicK